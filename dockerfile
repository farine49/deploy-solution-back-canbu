FROM maven:3.8.4 as build
COPY . ./
RUN mvn -DskipTests clean install

#
# Package stage
#
FROM openjdk:17
ENV DB_HOST="DBHOST" DB_USER="admin" DB_PASSWORD="DBPASS" DB_PORT="3306" DB_NAME="canbudak"
COPY --from=build /target/mds-dds-exam-spring-boot-server-0.0.1-SNAPSHOT.jar /src/local/lib/demo.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/src/local/lib/demo.jar"]